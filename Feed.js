import React, { Component } from "react";
import { StyleSheet, Text, View, FlatList } from "react-native";

import Avatar from "./Avatar";
import Detail from "./Detail";

export default class Feed extends React.Component {
  state = {};

  componentDidMount() {
    this.fetchImages();
  }

  fetchImages = async () => {
    const response = await fetch("https://dog.ceo/api/breeds/image/random/50");
    const { message } = await response.json();

    this.setState({
      images: message
    });
  };

  handleAvatarPress = (imageUrl) => {
    this.props.navigation.push("Detail", {imageUrl});
  }

  renderContent = () => {
    if(this.state.images) {
      return (
        <FlatList 
          data={this.state.images} 
          keyExtractor={(item, index) => index}
          renderItem={({item}) => <Avatar imageUrl={item} onPress={this.handleAvatarPress.bind(this, item)} />}
          numColumns={3}
        />
      );
    }
    return <Text>Loading...</Text>;
  };

  render() {
    return <View style={styles.container}>{this.renderContent()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  }
});
