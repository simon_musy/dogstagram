import React, { Component } from "react";
import { Dimensions } from "react-native";
import { StyleSheet, Image, TouchableOpacity } from "react-native";

const dimension = Dimensions.get("window").width / 3;

export default class Avatar extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} >
        <Image style={{width: dimension, height: dimension}} source={{uri: this.props.imageUrl}} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  image: {
      width: 200,
      height: 200
  }
});
